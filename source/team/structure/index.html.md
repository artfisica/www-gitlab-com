---
layout: markdown_page
title: Team Structure
page_class: org-structure
---

Draft team structure, this is to be finished after the Austin Summit (end of May 2016).

GitLab Inc. has at most four layers in the team structure:

1. CEO
1. Executives (e-team) consisting of CxO's and VP's
1. Leads
1. Individual contributors (IC's)

The indentation below reflects the reporting relations.
You can see our complete team and who reports to who on the [team page](https://about.gitlab.com/team/).
If there is a hyphen (-) in a line the part before hyphen is the name of the department and sometimes links to the relevant part of the [handbook](https://about.gitlab.com/handbook/).
The job titles (in _italics_) link to the job descriptions.
Our open vacancies are at our [jobs page](https://about.gitlab.com/jobs/).


- [**General**](/handbook/) - [_CEO_](/jobs/chief-executive-officer/)
  - [**Marketing**](/handbook/marketing/) - [_CMO_](/jobs/chief-marketing-officer/)
    - Design - [_Designer_](/jobs/designer/)
    - [Demand generation](/handbook/marketing/demand-generation) - [_Senior Demand Generation Manager_](/jobs/demand-generation-manager/) (lead)
      - [Online Marketing](/handbook/marketing/online-marketing) - [_Online Marketing Manager_](/jobs/online-marketing-manager/)
      - Business Development - [_Business Development Team Lead_](/jobs/business-development-team-lead/) (lead)
         - [_Business Development Representatives_](/jobs/business-development-representative/)
    - [Product Marketing](/handbook/marketing/product-marketing/) - [_Senior Product Marketing Manager_](/jobs/product-marketing-manager/) (lead)
      - [Partner Marketing](/handbook/marketing/product-marketing/#partnermarketing/) - _Partner/Channel Marketing Manager_ (vacancy)
      - [Content Marketing](/handbook/marketing/developer-relations/content-marketing/) - _Content Marketing Manager_ (vacancy)
    - Developer Relations
      - [Developer Advocacy](/handbook/marketing/developer-relations/developer-advocacy/) - [_Developer Advocate_](/jobs/developer-advocate/)
      - [Field Marketing](/handbook/marketing/developer-relations/field-marketing/) - [_Field Marketing Manager_](/jobs/field-marketing-manager/)
      - Technical Writing - [_Technical Writer_](/jobs/technical-writer/)
  - [**Sales**](/handbook/sales-process/) - [_CRO_](/jobs/chief-revenue-officer/)
    - Americas Sales - [_Account Executive_](/jobs/account-executive/)
    - EMEA Sales - [_Sales Director EMEA_](/jobs/sales-director/) (lead)
      - [_Account Executive_](/jobs/account-executive/)
    - APAC Sales - [_Director of Global Alliances and APAC Sales_](/jobs/director-of-global-alliances-and-apac-sales/) (lead)
    - Customer Success - _Customer Success Manager_ (lead, future vacancy)
      - [_Account Manager_](/jobs/account-manager/)
      - [_Solutions Engineer_](/jobs/solutions-engineer/)
  - [**Finance**](/handbook/accounting/) - [_CFO_](/jobs/chief-financial-officer/)
    - [Accounting](/handbook/accounting/) - [_Controller_](/jobs/controller/)
    - [People Operations](/handbook/people-operations/) - [_People Operations Director_](/jobs/people-ops-director/) (lead, vacancy)
      - [_People Operations Coordinator_](/jobs/people-ops-coordinator/)
      - [_Administrative Coordinator_](/jobs/adminstrative-coordinator/)
  - **Technical Direction** - [_CTO_](/jobs/chief-technology-officer/)
    - [_UX Designers_](/jobs/ux-designer/)
  - **Engineering** - [_VP of Engineering_](/jobs/vp-of-engineering/)
    - Various teams - [_Development Lead_](/jobs/development-lead/) (lead)
      - [_Developers_](/jobs/developer/) that are specialized in what the team does (CI, others)
    - Frontend - [_Frontend Lead_](/jobs/frontend-lead/) (lead)
      - [_Frontend engineer_](/jobs/frontend-engineer/)
    - [Infrastructure](/handbook/operations/) - [_Senior Production Engineer_](/jobs/production-engineer/) (lead)
      - [_Production Engineers_](/jobs/production-engineer/)
      - [_Developers_](/jobs/developer/) that are performance specialists
    - [Support](/handbook/support/) - [_Lead Service Engineer_](/jobs/service-engineer/) (lead)
      - [_Service Engineers_](/jobs/service-engineer/)
    - [_Developers_](/jobs/developer/) that work on other products
  - **Scaling** - [_VP of Scaling_](/jobs/vp-of-scaling/)
  - **General Product** - [_VP of Product_](/jobs/vice-president-of-product/)
  - **CI/CD Product** - [_Head of Product_](/jobs/head-of-product/)
