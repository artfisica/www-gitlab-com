---
layout: markdown_page
title: "GitLab Primer"
---

Read the pages below to learn more about GitLab:

1. [About](https://about.gitlab.com/about/)
1. [Strategy](https://about.gitlab.com/strategy/)
1. [Direction](https://about.gitlab.com/direction/)
1. [Handbook](https://about.gitlab.com/handbook/)
1. [Team](https://about.gitlab.com/team/)
