---
layout: markdown_page
title: "People Operations Administrator"
---

## Responsibilities

* Pro-actively improve People Operations activities and processes, 
and take on a very hands-on role in the day to day administration.
* Prepare contracts and help ensure smooth onboarding of new team members.
* Help ensure smooth offboarding of team members.
* Handle administrative interactions for US-based team members with our payroll and benefits provider, TriNet.
* Process changes to team members compensation, position, etc.
* Assist managers in delicate People Operations issues (special circumstances, conflicts, sickness, layoffs, etc.)
* Document and improve People Operations processes following the [GitLab workflow](https://about.gitlab.com/handbook/#gitlab-workflow),
with the goal always being to make things easier from the perspective of the team members
* Keep it efficient and [DRY](https://en.wikipedia.org/wiki/Don%27t_repeat_yourself).
* Provide assistance to the team with miscellaneous support tasks.
* Watch the hiring pipeline and ensure proper follow-up.
* Suggest improvements to the hiring pipeline, for example a better referral program.
* Initiate, design, and implement training programs.
* Write job descriptions and promotion criteria.
* Help the CEO answer team member [Feedback Form](https://about.gitlab.com/culture/) questions.

## Requirements

* Experience with US employment law and best practices
* Excellent written and verbal communication skills
* Enthusiasm for and broad experience with software tools
* Proven experience in quickly learning new software tools
* Willing to work with git and GitLab whenever possible
* Willing to make People Operations as open and transparent as possible

